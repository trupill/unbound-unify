{-# language GADTs,
             ScopedTypeVariables,
             MultiParamTypeClasses,
             FlexibleContexts,
             FlexibleInstances #-}
module Unbound.Generics.LocallyNameless.Unify.MetaVar where

import Data.Typeable
import Unbound.Generics.LocallyNameless

data MetaVarCapture a = CaptureNone | CaptureOnly [Name a] | CaptureAny
               deriving (Eq, Ord)   
data MetaVar a = MetaVar { mvName :: Name a, mvCapture :: MetaVarCapture a }

newMetaVar :: String -> MetaVarCapture a -> MetaVar a
newMetaVar x = MetaVar (s2n x)

freshMetaVar :: Fresh m => String -> MetaVarCapture a -> m (MetaVar a)
freshMetaVar x cp = MetaVar <$> fresh (s2n x) <*> pure cp

instance Eq (MetaVar a) where
  x == y = mvName x == mvName y
instance Ord (MetaVar a) where
  compare x y = compare (mvName x) (mvName y)
instance Show (MetaVar a) where
  show (MetaVar v _) = show v
instance Typeable a => Alpha (MetaVar a) where
  -- MetaVars are only alpha-equiv if they are the same
  aeq' ctx x y
    | isTermCtx ctx = mvName x == mvName y
    | otherwise     = False
  acompare' ctx x y = acompare' ctx (mvName x) (mvName y)
  -- MetaVars have no free variables and are only terms
  fvAny' _ _ = pure
  isTerm _ = mempty
  isPat  _ = inconsistentDisjointSet
  namePatFind _ = mempty
  nthPatFind  _ = mempty
  -- Since there are no free variables, there's no manipulation to be done
  freshen'  ctx m = return (m, mempty)
  lfreshen' ctx m cont = cont m mempty
  swaps' ctx perm m = m
  open   ctx b    m = m
  close  ctx b    m = m
instance Subst (MetaVar b) a where
  -- MetaVars are never real vars
  subst  _ _ = id
  substs _ = id
instance Subst a (MetaVar b) where
  -- MetaVars are never real vars
  subst  _ _ = id
  substs _ = id

data AnyMetaVar where
  AnyMetaVar :: Typeable a => MetaVar a -> AnyMetaVar

instance Show AnyMetaVar where
  show (AnyMetaVar x) = show x
instance Eq AnyMetaVar where
  AnyMetaVar x == AnyMetaVar y
    = case gcast x of
        Just y' -> x == y'
        _       -> False
instance Ord AnyMetaVar where
  compare (AnyMetaVar x) (AnyMetaVar y)
    = case compare (typeOf x) (typeOf y) of
        EQ -> case gcast y of
                Just y' -> compare x y'
                Nothing -> error "This can never happen!"
        other -> other
instance Alpha AnyMetaVar where
  -- MetaVars are only alpha-equiv if they are the same
  aeq' ctx (AnyMetaVar x) (AnyMetaVar y)
    = case gcast y of
        Just y' -> aeq' ctx x y'
        _       -> False
  acompare' ctx (AnyMetaVar x) (AnyMetaVar y)
    = case compare (typeOf x) (typeOf y) of
        EQ -> case gcast y of
                Just y' -> acompare' ctx x y'
                Nothing -> error "This can never happen!"
        other -> other
  -- MetaVars have no free variables and are only terms
  fvAny' _ _ = pure
  isTerm _ = mempty
  isPat  _ = inconsistentDisjointSet
  namePatFind _ = mempty
  nthPatFind  _ = mempty
  -- Since there are no free variables, there's no manipulation to be done
  freshen'  ctx m = return (m, mempty)
  lfreshen' ctx m cont = cont m mempty
  swaps' ctx perm m = m
  open   ctx b    m = m
  close  ctx b    m = m
instance Subst AnyMetaVar a where
  -- MetaVars are never real vars
  subst  _ _ = id
  substs _ = id
instance Subst a AnyMetaVar where
  -- MetaVars are never real vars
  subst  _ _ = id
  substs _ = id