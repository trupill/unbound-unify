{-# language GADTs,
             TypeOperators,
             ScopedTypeVariables,
             MultiParamTypeClasses,
             DefaultSignatures,
             FlexibleContexts,
             FlexibleInstances #-}
module Unbound.Generics.LocallyNameless.Unify.MetaSubst where

import Data.List (unionBy)
import Data.Typeable
import GHC.Generics
import Unbound.Generics.LocallyNameless
import Unbound.Generics.LocallyNameless.Unify.MetaVar

data MetaSubstName a b where
  MetaSubstName :: a ~ b => MetaVar a -> MetaSubstName a b

class MetaSubst b a where
  isMetaVar :: a -> Maybe (MetaSubstName a b)
  isMetaVar _ = Nothing
  
  metaVars :: a -> [MetaVar b]
  default metaVars :: (Generic a, GMetaSubst b (Rep a)) => a -> [MetaVar b]
  metaVars x
    = case (isMetaVar x :: Maybe (MetaSubstName a b)) of
        Just (MetaSubstName m) -> [m]
        Nothing -> gMetaVars (from x)

  metaSubst :: MetaVar b -> b -> a -> a
  default metaSubst :: (Generic a, GMetaSubst b (Rep a)) => MetaVar b -> b -> a -> a
  metaSubst n u x
    = case (isMetaVar x :: Maybe (MetaSubstName a b)) of
        Just (MetaSubstName m) | m == n -> u
        _ -> to $ gMetaSubst n u (from x)

-- Generic substitution of metavars
class GMetaSubst b f where
  gMetaVars  :: f c -> [MetaVar b]
  gMetaSubst :: MetaVar b -> b -> f c -> f c
instance MetaSubst b c => GMetaSubst b (K1 i c) where
  gMetaVars = metaVars . unK1
  gMetaSubst nm val = K1 . metaSubst nm val . unK1
instance GMetaSubst b f => GMetaSubst b (M1 i c f) where
  gMetaVars = gMetaVars . unM1
  gMetaSubst nm val = M1 . gMetaSubst nm val . unM1
instance GMetaSubst b U1 where
  gMetaVars _ = []
  gMetaSubst _nm _val _ = U1
instance GMetaSubst b V1 where
  gMetaVars _ = []
  gMetaSubst _nm _val = id
instance (Typeable b, GMetaSubst b f, GMetaSubst b g) => GMetaSubst b (f :*: g) where
  gMetaVars (f :*: g) = unionBy aeq (gMetaVars f) (gMetaVars g)
  gMetaSubst nm val (f :*: g) = gMetaSubst nm val f :*: gMetaSubst nm val g
instance (GMetaSubst b f, GMetaSubst b g) => GMetaSubst b (f :+: g) where
  gMetaVars (L1 f) = gMetaVars f
  gMetaVars (R1 g) = gMetaVars g
  gMetaSubst nm val (L1 f) = L1 $ gMetaSubst nm val f
  gMetaSubst nm val (R1 g) = R1 $ gMetaSubst nm val g
-- primitive types
instance MetaSubst b Int     where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst b Integer where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst b Bool    where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst b ()      where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst b Char    where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst b Float   where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst b Double  where metaVars _ = [] ; metaSubst _ _ = id
-- basic types
instance (Typeable c, MetaSubst c a, MetaSubst c b) => MetaSubst c (a,b)
instance (Typeable c, MetaSubst c a, MetaSubst c b, MetaSubst c d) => MetaSubst c (a,b,d)
instance (Typeable c, MetaSubst c a, MetaSubst c b, MetaSubst c d, MetaSubst c e) => MetaSubst c (a,b,d,e)
instance (Typeable c, MetaSubst c a, MetaSubst c b, MetaSubst c d, MetaSubst c e, MetaSubst c f) => MetaSubst c (a,b,d,e,f)
instance (Typeable c, MetaSubst c a) => MetaSubst c [a]
instance (Typeable c, MetaSubst c a) => MetaSubst c (Maybe a)
instance (Typeable c, MetaSubst c a, MetaSubst c b) => MetaSubst c (Either a b)
-- variables
instance MetaSubst b (MetaVar a) where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst b AnyMetaVar  where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst b (Name a)    where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst b AnyName     where metaVars _ = [] ; metaSubst _ _ = id
instance MetaSubst a (Ignore b)  where metaVars _ = [] ; metaSubst _ _ = id
-- binders
instance (MetaSubst c a) => MetaSubst c (Embed a)
instance (MetaSubst c e) => MetaSubst c (Shift e) where
  metaVars (Shift e) = metaVars e
  metaSubst x b (Shift e) = Shift (metaSubst x b e)
instance (Typeable c, MetaSubst c b, MetaSubst c a, Alpha a, Alpha b) => MetaSubst c (Bind a b)
instance (Typeable c, MetaSubst c p1, MetaSubst c p2) => MetaSubst c (Rebind p1 p2)
instance (MetaSubst c p) => MetaSubst c (Rec p)
instance (Typeable c, Alpha p, MetaSubst c p) => MetaSubst c (TRec p)