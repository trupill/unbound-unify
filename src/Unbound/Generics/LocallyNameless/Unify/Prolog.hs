{-# language ConstraintKinds,
             GeneralizedNewtypeDeriving,
             MultiParamTypeClasses,
             FlexibleInstances #-}
module Unbound.Generics.LocallyNameless.Unify.Prolog (
  module Control.Monad.Logic.Class
, module Unbound.Generics.LocallyNameless.Unify
, PrologM, observeP, observeManyP, observeAllP
, unifyP, exists
) where

import Control.Applicative
import Control.Monad.Except
import Control.Monad.Logic
import Control.Monad.Logic.Class
import Control.Monad.State
import Unbound.Generics.LocallyNameless.Unify

instance Fresh m => Fresh (LogicT m) where
  fresh = lift . fresh

newtype PrologM t a = PrologM { unPrologM :: StateT (Context t) (LogicT FreshM) a }
                    deriving ( Functor, Applicative, Monad, Alternative
                             , MonadPlus, MonadLogic, MonadState (Context t), Fresh )

instance MonadError (UnificationError t) (PrologM t) where
  throwError _ = empty
  catchError x next = x <|> next YourMonadDoesNotSupportSpecificErrors

observeP :: PrologM t a -> (a, Context t)
observeP = runFreshM . observeT . flip runStateT [] . unPrologM
observeManyP :: Int -> PrologM t a -> [(a, Context t)]
observeManyP i = runFreshM . observeManyT i . flip runStateT [] . unPrologM
observeAllP :: PrologM t a -> [(a, Context t)]
observeAllP = runFreshM . observeAllT . flip runStateT [] . unPrologM

unifyP :: Unif t => t -> t -> PrologM t ()
unifyP = unifyWithRulesP []

unifyWithRulesP :: Unif t => [UnificationRule (PrologM t) t] -> t -> t -> PrologM t ()
unifyWithRulesP rules x y = do
  ctx <- get
  let x' = applyContext ctx x
      y' = applyContext ctx y
  ctx' <- unifyWithRulesAndContext rules ctx x' y'
  put ctx'

exists :: Fresh m => String -> MetaVarCapture t -> (MetaVar t -> m a) -> m a
exists x cp f = freshMetaVar x cp >>= f