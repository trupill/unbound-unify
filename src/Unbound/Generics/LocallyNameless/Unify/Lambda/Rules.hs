{-# language ViewPatterns,
             FlexibleContexts #-}
module Unbound.Generics.LocallyNameless.Unify.Lambda.Rules where

import Control.Monad
import Control.Monad.State
import Control.Monad.Writer
import Data.Either (isRight)
import Data.List (permutations, nub, sort)
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import GHC.Stack (HasCallStack)
import Unbound.Generics.LocallyNameless.Unify
import Unbound.Generics.LocallyNameless.Unify.Lambda

import Debug.Trace (trace)

allRules :: ( UnificationM m LambdaTerm
            , MonadWriter [(String, [LambdaTerm])] m
            , MonadState LetBindings m )
         => [UnificationRule m LambdaTerm]
allRules = [concatMapRule, etaExpandRule, letIntroRule, bothLetUseRule, useALocalEqRule]

unifyLambdaTerm :: HasCallStack
                => LambdaTerm -> LambdaTerm 
                -> Either (UnificationError LambdaTerm)
                          (Context LambdaTerm, [(String, [LambdaTerm])])
unifyLambdaTerm = unifyWithRulesStateAndLog allRules emptyLetBindings

concatMapRule :: (UnificationM m LambdaTerm, MonadWriter [(String, [LambdaTerm])] m)
              => UnificationRule m LambdaTerm
concatMapRule p@(lhs :=: rhs)
  | (Const "concatMap", [f, xs]) <- split lhs
  , (_, [_]) <- split rhs
  = do tell [("concatMap f xs -> concat (map f xs)", [f, xs])]
       singleProblem $ App (Const "concat") (unsplit (Const "map") [f, xs]) :=: rhs
  | otherwise
  = ruleDoesNotApplyHere p

etaExpandRule :: (UnificationM m LambdaTerm, MonadWriter [(String, [LambdaTerm])] m)
              => UnificationRule m LambdaTerm
etaExpandRule p@(lhs :=: rhs)
  | Lam _ <- lhs, Lam _ <- rhs
  = ruleDoesNotApplyHere p
  | Lam _ <- rhs
  = do tell [("eta-expand", [lhs])]
       x <- fresh (s2n "x")
       singleProblem $ Lam (bind x (App lhs (Var x))) :=: rhs
  | otherwise
  = ruleDoesNotApplyHere p

data LetBindings = LB { lbCode      :: M.Map (Name LambdaTerm) LambdaTerm
                      , lbVisited   :: S.Set (Name LambdaTerm)
                      , lbConstArgs :: M.Map (Name LambdaTerm) ([Int], [LambdaTerm])
                      , lbEqs       :: [UnificationProblem LambdaTerm] }
                 deriving (Show)

emptyLetBindings :: LetBindings
emptyLetBindings = LB M.empty S.empty M.empty []

insertLetBinding :: Name LambdaTerm -> LambdaTerm
                 -> LetBindings -> LetBindings
insertLetBinding x f lb = lb { lbCode = M.insert x f (lbCode lb) }

letIntroRule :: (UnificationM m LambdaTerm, MonadState LetBindings m)
             => UnificationRule m LambdaTerm
letIntroRule (Let b :=: rhs)
  = do (unrec -> xs, e) <- unbind b
       forM_ xs $ \(x, unembed -> t) -> modify (insertLetBinding x t)
       singleProblem $ e :=: rhs
letIntroRule (lhs :=: Let b)
  = do (unrec -> xs, e) <- unbind b
       forM_ xs $ \(x, unembed -> t) -> modify (insertLetBinding x t)
       singleProblem $ lhs :=: e
letIntroRule p = ruleDoesNotApplyHere p

generatePossiblePermutations
  :: (UnificationM m LambdaTerm, HasCallStack)
  => Int -> Int -> m [([MetaVar LambdaTerm], [MetaVar LambdaTerm])]
generatePossiblePermutations n1 n2
  | n1 == n2
  = do let n = n1 -- Just make it clear it's the same
       metavars <- replicateM n (freshMetaVar "X" CaptureAny)
       let ps = permutations metavars
       return $ [(metavars, p) | p <- ps]
  | otherwise
  = error "Not supported at this moment"

bothLetUseRule :: ( UnificationM m LambdaTerm
                  , MonadState LetBindings m
                  , MonadWriter [(String, [LambdaTerm])] m
                  , HasCallStack )
               => UnificationRule m LambdaTerm
bothLetUseRule p@(lhs :=: rhs)
  | (Var f, args1) <- split lhs
  , (Var g, args2) <- split rhs
  , length args1 == length args2
  = do code <- gets lbCode
       vist <- gets lbVisited
       case (code M.!? f, f `S.member` vist, code M.!? g, g `S.member` vist) of
         (Just fCode, False, Just gCode, False)
           -> do let n = length args1 -- or length args2
                 perms <- generatePossiblePermutations n n
                 return $ flip map perms $ \(p1, p2) -> do  -- For each possible perm.
                   let e1 = unsplit (Var f) (map Meta p1)
                       e2 = unsplit (Var g) (map Meta p2)
                   if isRight $ unify (App lhs rhs) (App e1 e2)
                      then do -- Add as visited, add equation
                              tell [("assumed eq", [e1, e2])]
                              lb <- get
                              let lb1 = lb  { lbVisited = S.insert f (S.insert g (lbVisited lb)) }
                                  lb2 = lb1 { lbEqs = (e1 :=: e2) : lbEqs lb1 }
                              put lb2
                              -- Now let's check that the bodies are OK
                              fCode' <- etaExpandUpTo n fCode
                              -- Swap the arguments too
                              gCode' <- etaExpandUpTo n gCode >>= swapLambdas p1 p2
                              -- Now let's introduce all the lambdas
                              (_, fBody, _, gBody) <- unabstract2Many n fCode' gCode'
                              return [fBody :=: gBody]
                      else ruleDoesNotApplyHere p
         _ -> ruleDoesNotApplyHere p
  | (Var f, args1) <- split lhs
  , (Var g, args2) <- split rhs
  , length args2 > length args1
  = do code <- gets lbCode
       vist <- gets lbVisited
       case (code M.!? f, f `S.member` vist, code M.!? g, g `S.member` vist) of
         (Just _, False, Just gCode, False)
           -> do -- Try to keep some of the things constant
                 let nToDrop = length args2 - length args1
                     -- The most inefficient way to find all possible args to drop
                     possibilities = nub $ map (sort . take nToDrop) (permutations [0 .. length args2 - 1])
                 return $ flip map possibilities $ \argsToDrop -> do
                   names <- replicateM (length args1) (fresh (s2n "x"))
                   let newArgs2 = constantifyArgs names args2 argsToDrop 0
                       droppedArgs2 = getArgs newArgs2 argsToDrop 0
                       gCode' = abstractMany names (unsplit gCode newArgs2)
                   gCodeN <- nf gCode'
                   tell [("drop args", [unsplit (Var g) newArgs2])]
                   -- Update with the new code for g
                   lb <- get
                   let lb1 = lb  { lbCode = M.insert g gCodeN (lbCode lb) }
                       lb2 = lb1 { lbConstArgs = M.insert g (argsToDrop, droppedArgs2) (lbConstArgs lb1) }
                   put lb2
                   -- Finally, just drop the ones we want from g
                   let newRhs = unsplit (Var g) (dropArgs args2 argsToDrop 0)
                   return [lhs :=: newRhs]
         _ -> ruleDoesNotApplyHere p 
  | otherwise
  = ruleDoesNotApplyHere p

constantifyArgs :: HasCallStack
                => [Name LambdaTerm] -> [LambdaTerm] -> [Int]
                -> Int -> [LambdaTerm]
constantifyArgs varNames [] _ _
  | [] <- varNames
  = []
  | (_:_) <- varNames
  = error "The list of var names should include exactly the ones I need, not more"
constantifyArgs varNames (originalA:originalAs) toDrop n
  | n `elem` toDrop
  = originalA : constantifyArgs varNames originalAs toDrop (n+1)
  | v:vs <- varNames
  = Var v : constantifyArgs vs originalAs toDrop (n+1)
  | [] <- varNames
  = error "The list of var names should include exactly the ones I need, not fewer"

getArgs :: [LambdaTerm] -> [Int] -> Int -> [LambdaTerm]
getArgs [] _ _ = []
getArgs (x:xs) argsToDrop n
  | n `elem` argsToDrop = x : getArgs xs argsToDrop (n+1)
  | otherwise           =     getArgs xs argsToDrop (n+1)

dropArgs :: [LambdaTerm] -> [Int] -> Int -> [LambdaTerm]
dropArgs [] _ _ = []
dropArgs (x:xs) argsToDrop n
  | n `elem` argsToDrop =     dropArgs xs argsToDrop (n+1)
  | otherwise           = x : dropArgs xs argsToDrop (n+1)

useALocalEqRule :: ( UnificationM m LambdaTerm
                   , MonadState LetBindings m
                   , MonadWriter [(String, [LambdaTerm])] m )
                => UnificationRule m LambdaTerm
useALocalEqRule p@(lhs :=: rhs)
  | (Var _, args1) <- split lhs
  , (Var g, args2) <- split rhs
  , length args2 > length args1
  = do -- In this case we have to drop the arguments
       drp <- gets lbConstArgs
       case drp M.!? g of
         Nothing -> ruleDoesNotApplyHere p
         Just (argsToDrop, thingsToEquate)
                 -> do let keptArgs    = dropArgs args2 argsToDrop 0
                           removedArgs = getArgs args2 argsToDrop 0
                       -- The constant parts should be equal to whatever was there
                           newEq = lhs :=: unsplit (Var g) keptArgs
                       return [return (newEq : zipWith (:=:) thingsToEquate removedArgs)]
  | (Var f, args1) <- split lhs
  , (Var g, args2) <- split rhs
  , length args1 == length args2
  = do eqs <- gets lbEqs
       return $ flip map eqs $ \(lEq :=: rEq) ->
         case (unify lEq lhs, unify rEq rhs) of
           (Right ctxL, Right ctxR) -> do
              tell [("used eq", [lEq, rEq])]
              return [l :=: r | (x, l) <- ctxL, let Just r = lookup x ctxR]
           _ -> ruleDoesNotApplyHere p
  | otherwise
  = ruleDoesNotApplyHere p