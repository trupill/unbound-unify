module Unbound.Generics.LocallyNameless.Unify.Lambda.Examples where

import Unbound.Generics.LocallyNameless.Unify.Lambda
import Unbound.Generics.LocallyNameless.Unify.Prolog

varX, varY, varZ, varW, varT :: Name LambdaTerm
varX = s2n "x"
varY = s2n "y"
varZ = s2n "z"
varW = s2n "w"
varT = s2n "t"

metaX, metaY :: LambdaTerm
metaX = Meta $ newMetaVar "X" CaptureNone
metaY = Meta $ newMetaVar "Y" CaptureNone
metaZ = Meta $ newMetaVar "Z" CaptureNone

termA, termB, termC, termD, termE :: LambdaTerm
termA = Lam (bind varX (App (App (Const "+") (Var varX)) (Const "0")))
termA' = Lam (bind varX (App (App (Const "+") (unsplit metaX [Var varX])) (Const "0")))
termB = Lam (bind varX (App (unsplit metaX [Var varX]) (Var varX)))
termC = Lam (bind varX (App (unsplit metaY [Var varX]) (App (Var varX) (Const "0"))))

termD = Lam $ bind varX $ Lam $ bind varY $ unsplit (Const "concatMap") [Var varX, Var varY]
termD' = Lam $ bind varX $ unsplit (Const "concatMap") [Var varX]
termE = Lam $ bind varX $ Lam $ bind varY $ App (Const "concat") (unsplit metaX [Var varX, Var varY])
termE' = Let $ bind (rec [(varZ, embed $ Const "0")]) $
         Lam $ bind varX $ Lam $ bind varY $ App (Const "concat") (unsplit metaX [Var varX, Var varY])

-- let x = e in t
simpleLet :: Name LambdaTerm -> LambdaTerm -> LambdaTerm -> LambdaTerm
simpleLet x e t = Let $ bind (rec [(x, embed e)]) t

letTermA, letTermB, letTermC :: LambdaTerm
letTermA = simpleLet varZ
             (Lam $ bind varY $ Const "0")
             (Lam $ bind varX $ Var varZ)
letTermB = simpleLet varZ
             (Lam $ bind varY $ Const "0")
             (Lam $ bind varX $ Lam $ bind varW $ App (Var varZ) (Var varW))
letTermC = simpleLet varZ
             (Lam $ bind varY $ metaZ)
             (Lam $ bind varX $ Lam $ bind varW $ App (Var varZ) (Var varW))


letTermD, letTermE, letTermF :: LambdaTerm
letTermD = simpleLet varW
             (Lam $ bind varX $ Lam $ bind varY $ unsplit (Const "f") [Var varX, Var varY])
             (Lam $ bind varZ $ unsplit (Var varW) [Var varZ, Const "0"])
letTermE = simpleLet varW
             (Lam $ bind varX $ Lam $ bind varY $ unsplit (Const "f") [Var varY, Var varX])
             (Lam $ bind varZ $ unsplit (Var varW) [Const "0", Var varZ])
letTermF = simpleLet varW
             (Lam $ bind varX $ Lam $ bind varY $ unsplit (Const "f")
                [unsplit metaX [Var varX, Var varY, Var varW], Var varX])
             (Lam $ bind varZ $ unsplit (Var varW) [Const "0", Var varZ])
letTermG = simpleLet varW
             (Lam $ bind varX $ Lam $ bind varY $ Lam $ bind varT $ unsplit (Const "f") [Var varY, Var varX])
             (Lam $ bind varZ $ unsplit (Var varW) [Const "0", Var varZ, Var varZ])

exampleProlog :: PrologM LambdaTerm ()
exampleProlog = exists "x" CaptureNone $ \x -> exists "y" CaptureNone $ \y -> do
                unifyP (Meta x) (Meta y)
                unifyP (Meta y) (Const "0")