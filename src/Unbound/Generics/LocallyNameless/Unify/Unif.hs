{-# language ScopedTypeVariables,
             MultiParamTypeClasses,
             FlexibleContexts,
             FlexibleInstances,
             MultiWayIf,
             TupleSections,
             ConstraintKinds,
             DeriveGeneric #-}
module Unbound.Generics.LocallyNameless.Unify.Unif (
  -- * Type classes for terms
  NormalForm(..), Unif(..), abstractMany,
  unabstractMany, unabstract2Many,
  -- * Simple unification of terms
  unify,
  UnificationProblem(..), Context, applyContext,
  UnificationError(..), UnificationErrorReason(..),
  -- * Unification with additional rules
  -- ** Definition of rules
  UnificationM, UnificationRule,
  ruleDoesNotApplyHere, singleProblem,
  -- ** Basic ways to run the algorithm within a monad
  unifyWithRules, unifyWithRulesAndContext,
  -- ** Useful ways to run the unification algorithm
  unifyWithRules', unifyWithRulesState,
  unifyWithRulesLog, unifyWithRulesStateAndLog
) where

import Control.Monad
import Control.Monad.Except
import Control.Monad.State
import Control.Monad.Writer
import Control.Monad.RWS
import Data.List (intersectBy, deleteFirstsBy, nub)
import Data.Maybe (isJust, mapMaybe)
import Data.Typeable
import GHC.Generics
import GHC.Stack (HasCallStack)
import Unbound.Generics.LocallyNameless
import Unbound.Generics.LocallyNameless.Internal.Fold
import Unbound.Generics.LocallyNameless.Unify.MetaSubst
import Unbound.Generics.LocallyNameless.Unify.MetaVar

-- Type classes for terms

class Alpha a => NormalForm a where
  nf :: Fresh m => a -> m a
  nf = return

class (Typeable a, Alpha a, Subst a a, MetaSubst a a, NormalForm a) => Unif a where
  split :: a -> (a, [a])
  unsplit :: a -> [a] -> a
  unsplitMeta :: MetaVar a -> [a] -> a
  abstract :: Bind (Name a) a -> a
  unabstract :: a -> Maybe (Bind (Name a) a)

abstractMany :: Unif a => [Name a] -> a -> a
abstractMany []     t = t
abstractMany (x:xs) t = abstract (bind x (abstractMany xs t))

unabstractMany :: (Fresh m, Unif a, HasCallStack)
               => Int -> a -> m ([Name a], a)
unabstractMany n t
  | n <= 0
  = return ([], t)
  | Just b <- unabstract t
  = do (x, e) <- unbind b
       (xs, e') <- unabstractMany (n-1) e
       return (x:xs, e')
  | otherwise
  = error $ "Cannot unabstract " ++ show n ++ " binders"

unabstract2Many :: (Fresh m, Unif a, HasCallStack)
                => Int -> a -> a -> m ([Name a], a, [Name a], a)
unabstract2Many n t1 t2
  | n <= 0
  = return ([], t1, [], t2)
  | Just b1 <- unabstract t1, Just b2 <- unabstract t2
  = do r <- unbind2 b1 b2
       case r of
         Nothing -> error $ "Cannot unabstract " ++ show n ++ " simultaneous binders (error in unbind2)"
         Just (x1, e1, x2, e2) -> do (xs1, e1', xs2, e2') <- unabstract2Many (n-1) e1 e2
                                     return (x1:xs1, e1', x2:xs2, e2')
  | otherwise
  = error $ "Cannot unabstract " ++ show n ++ " simultaneous binders (one is not a binder)"

-- Definition of unification problems

infix 5 :=:
data UnificationProblem a = a :=: a deriving (Show, Eq, Ord, Generic)
instance (Typeable a, MetaSubst a a) => MetaSubst a (UnificationProblem a)

type Context a = [(MetaVar a, a)]
applyContext :: MetaSubst b a => Context b -> a -> a
applyContext eqs t = foldr (uncurry metaSubst) t eqs

data UnificationError a
  = UnificationError { ueLhs :: a, ueRhs :: a
                     , ueReason :: UnificationErrorReason }
  | YourMonadDoesNotSupportSpecificErrors
  deriving (Show, Eq)
data UnificationErrorReason
  = DifferentHeadConstructor
  | DifferentNumberOfArgs
  | NotEnoughBinders
  | FlexibleHeadWithNonVariableArgs
  | MetaVarCycle
  | VariableCapture
  | RuleDoesNotApplyHere
  deriving (Show, Eq)
type UnificationResult a = Either (UnificationError a) (Context a)

-- Definition of unification rules

type UnificationM m a = (Fresh m, MonadError (UnificationError a) m)

-- Outer list -> or, inner list -> and
type UnificationRule m a = UnificationProblem a -> m [m [UnificationProblem a]]
ruleDoesNotApplyHere :: UnificationM m a => UnificationProblem a -> m b
ruleDoesNotApplyHere (x :=: y) = throwError $ UnificationError x y RuleDoesNotApplyHere
singleProblem :: Monad m => UnificationProblem a -> m [m [UnificationProblem a]]
singleProblem p = return [ return [p] ]

-- Different useful ways to run the unification process

unify :: (Unif a, HasCallStack)
      => a -> a -> UnificationResult a
unify x y = runFreshM $ runExceptT $ unifyWithRules [] x y

unifyWithRules' :: (Unif a, HasCallStack)
                => [UnificationRule (ExceptT (UnificationError a) FreshM) a]
                -> a -> a -> UnificationResult a
unifyWithRules' rules x y = runFreshM $ runExceptT $ unifyWithRules rules x y

unifyWithRulesState
  :: (Unif a, HasCallStack)
  => [UnificationRule (StateT s (ExceptT (UnificationError a) FreshM)) a]
  -> s -> a -> a -> UnificationResult a
unifyWithRulesState rules initialState x y
  = runFreshM $ runExceptT $ flip evalStateT initialState $ unifyWithRules rules x y

unifyWithRulesLog
  :: (Unif a, Monoid l, HasCallStack)
  => [UnificationRule (WriterT l (ExceptT (UnificationError a) FreshM)) a]
  -> a -> a -> Either (UnificationError a) (Context a, l)
unifyWithRulesLog rules x y
  = runFreshM $ runExceptT $ runWriterT $ unifyWithRules rules x y

unifyWithRulesStateAndLog
  :: (Unif a, Monoid l, HasCallStack)
  => [UnificationRule (RWST () l s (ExceptT (UnificationError a) FreshM)) a]
  -> s -> a -> a -> Either (UnificationError a) (Context a, l)
unifyWithRulesStateAndLog rules initialState x y
  = runFreshM $ runExceptT $ evalRWST (unifyWithRules rules x y) () initialState

-- THE UNIFICATION ALGORITHM ITSELF

unifyWithRules :: forall m a. (Unif a, UnificationM m a, HasCallStack)
               => [UnificationRule m a] -> a -> a -> m (Context a)
unifyWithRules rules x y = unifyWithRulesAndContext rules [] x y

unifyWithRulesAndContext
  :: forall m a. (Unif a, UnificationM m a, HasCallStack)
  => [UnificationRule m a] -> Context a -> a -> a -> m (Context a)
unifyWithRulesAndContext rules ctx x y = unify' [x :=: y] ctx
  where
    unify' :: [UnificationProblem a] -> Context a -> m (Context a)
    unify' [] ctx = do
      -- Final normalisation
      ctx' <- forM ctx $ \(f, fval) -> (f,) <$> nf fval
      return ctx'
    unify' ((x :=: y):rest) ctx = do
      x' <- nf x
      y' <- nf y
      ctx' <- forM ctx $ \(f, fval) -> (f,) <$> nf fval
      unify1 ((x' :=: y'):rest) ctx

    -- Rules taken from http://cl-informatik.uibk.ac.at/teaching/ws17/vs/slides/P2.pdf
    unify1 :: [UnificationProblem a] -> Context a -> m (Context a)
    unify1 [] ctx = error "This should have been caught by unify' :/"
    unify1 (p@(x :=: y):rest) ctx
      -- Rule 0: t = t => [], {}
      | aeq x y
      = unify' rest ctx
      -- Rule 1: \x. s = \x. t => [s = t], {}
      | Just x' <- unabstract x, Just y' <- unabstract y
      = do r <- unbind2 x' y'
           case r of
             Just (_xv, xe, _yv, ye) -> unify' ((xe :=: ye):rest) ctx
             Nothing -> throwError $ UnificationError x y NotEnoughBinders
      -- Rules about constructors
      | otherwise
      = do let ((xh, xargs), (yh, yargs)) = (split x, split y)
           case (isMetaVar xh :: Maybe (MetaSubstName a a),
                 isMetaVar yh :: Maybe (MetaSubstName a a)) of
             -- Rule 2: decompose constructors
             -- Here we require heads to be alpha-equivalent
             (Nothing, Nothing)
               | not (xh `aeq` yh)
               -> tryWithRules rules p DifferentHeadConstructor rest ctx
               | not (length xargs == length yargs)
               -> tryWithRules rules p DifferentNumberOfArgs rest ctx
               | otherwise
               -> unify' (zipWith (:=:) xargs yargs ++ rest) ctx
             -- Rule 3: MetaVar head + non-meta head
             (Just (MetaSubstName xmeta), Nothing) -> do
               ((f, fval), problems) <- twoOfDifferentKind p (xmeta, xargs) (yh, yargs)
               unify' (problems ++ metaSubst f fval rest) ((f, fval) : metaSubstCtx f fval ctx)
             (Nothing, Just (MetaSubstName ymeta)) -> do
               ((f, fval), problems) <- twoOfDifferentKind p (ymeta, yargs) (xh, xargs)
               unify' (problems ++ metaSubst f fval rest) ((f, fval) : metaSubstCtx f fval ctx)
              -- Rules 4 and 5: both heads are MetaVars
             (Just (MetaSubstName xmeta), Just (MetaSubstName ymeta))
               -- Rule 4: same MetaVar in the head
               | xmeta == ymeta
               -> do -- All variables, and same number of args
                     let xsns = map (isvar :: a -> Maybe (SubstName a a)) xargs
                         ysns = map (isvar :: a -> Maybe (SubstName a a)) yargs
                     if | not (length xargs == length yargs)
                        -> throwError $ UnificationError x y DifferentNumberOfArgs
                        | not (all isJust xsns && all isJust ysns)
                        -> throwError $ UnificationError x y FlexibleHeadWithNonVariableArgs
                        | otherwise
                        -> do -- Generate the inner term
                              h <- freshMetaVar "H" CaptureNone
                              let xargs' = map ((\(SubstName x) -> x) . fromJust') xsns
                                  -- We should only keep the shared variables
                                  hArgs  = mapMaybe (\(x,y) -> if x `aeq` y then Just x else Nothing)
                                                    (zip xargs yargs)
                                  xval   = abstractMany xargs' $ unsplitMeta h hArgs
                              unify' (metaSubst xmeta xval rest) 
                                     ((xmeta,xval) : metaSubstCtx xmeta xval ctx)
               -- Rule 5: different metavar in head
               | otherwise
               -> do -- All variables
                     let xsns = map (isvar :: a -> Maybe (SubstName a a)) xargs
                         ysns = map (isvar :: a -> Maybe (SubstName a a)) yargs
                     if | not (all isJust xsns && all isJust ysns)
                        -> throwError $ UnificationError x y FlexibleHeadWithNonVariableArgs
                        | otherwise
                        -> do -- Generate the inner term
                              h <- freshMetaVar "H" CaptureNone
                              let -- We should only keep the shared variables
                                  hArgs  = intersectBy (aeq) xargs yargs
                                  -- Build for x
                                  xargs' = map ((\(SubstName x) -> x) . fromJust') xsns
                                  xval   = abstractMany xargs' $ unsplitMeta h hArgs
                                  -- Build for y
                                  yargs' = map ((\(SubstName x) -> x) . fromJust') ysns
                                  yval   = abstractMany yargs' $ unsplitMeta h hArgs
                                  -- Susbtitution to apply
                                  substRest = metaSubst ymeta yval $ metaSubst xmeta xval rest
                                  substCtx  = metaSubstCtx ymeta yval $ metaSubstCtx xmeta xval ctx
                              unify' substRest ((ymeta,yval) : (xmeta,xval) : substCtx)

    twoOfDifferentKind :: UnificationProblem a -> (MetaVar a, [a]) -> (a, [a])
                       -> m ((MetaVar a, a), [UnificationProblem a])
    twoOfDifferentKind (x :=: y) (xmeta, xargs) (yh, yargs)
      | let sns = map (isvar :: a -> Maybe (SubstName a a)) xargs
      , any (not . isJust) sns
      = throwError $ UnificationError x y FlexibleHeadWithNonVariableArgs
      | xmeta `elem` metaVars yargs
      = throwError $ UnificationError x y MetaVarCycle
      | otherwise
      = do let fvsYHead = fvs' yh
               xvars = map ((\(SubstName x) -> x) . fromJust' . (isvar :: a -> Maybe (SubstName a a))) xargs
               outOfScope = deleteFirstsBy aeq (nub fvsYHead) (nub xvars) 
           case mvCapture xmeta of
             CaptureNone | not (null outOfScope)
                         -> throwError $ UnificationError x y VariableCapture
             CaptureOnly vars | not (null $ intersectBy aeq outOfScope vars)
                         -> throwError $ UnificationError x y VariableCapture
             _ -> do hs <- replicateM (length yargs) (freshMetaVar "H" CaptureNone)
                     let neweqs = [unsplitMeta h xargs :=: yarg | (h, yarg) <- zip hs yargs]
                         xval   = abstractMany xvars (unsplit yh [x | x :=: _ <- neweqs])
                     return ((xmeta, xval), neweqs)

    -- Try with each rule
    tryWithRules :: [UnificationRule m a] -> UnificationProblem a
                 -> UnificationErrorReason
                 -> [UnificationProblem a] -> Context a -> m (Context a)
    tryWithRules [] (x :=: y) reason _ _
      = throwError $ UnificationError x y reason
    tryWithRules (r:rs) p reason rest ctx
      = (do nexts <- r p
            tryWithRules' p nexts rest ctx)
        `catchError`
        (\_ -> tryWithRules rs p reason rest ctx)
    -- Try with each possibility of the rules
    tryWithRules' :: UnificationProblem a -> [m [UnificationProblem a]]
                  -> [UnificationProblem a] -> Context a -> m (Context a)
    tryWithRules' (x :=: y) [] _ _
      = throwError $ UnificationError x y RuleDoesNotApplyHere
    tryWithRules' p (next:nexts) rest ctx
      = (do next' <- next
            unify' (next' ++ rest) ctx)
        `catchError`
        (\_ -> tryWithRules' p nexts rest ctx)

    metaSubstCtx :: MetaSubst b a => MetaVar b -> b -> Context a -> Context a
    metaSubstCtx f fval ctx = [(x, metaSubst f fval xval) | (x, xval) <- ctx]

-- Utilities

fromJust' :: HasCallStack => Maybe a -> a
fromJust' (Just x) = x
fromJust' _ = error "Oh, a Just was expected"

fvs' :: (Alpha a, Typeable a) => a -> [Name a]
fvs' = toListOf fv

instance (Monoid w, Fresh m) => Fresh (RWST r w s m) where
  fresh = lift . fresh