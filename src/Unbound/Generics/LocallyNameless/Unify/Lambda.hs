{-# language DeriveGeneric,
             MultiParamTypeClasses,
             FlexibleContexts,
             ViewPatterns,
             ScopedTypeVariables #-}
module Unbound.Generics.LocallyNameless.Unify.Lambda where

import Control.Monad (forM, replicateM)
import Data.List (elemIndex, (!!))
import Data.Maybe (catMaybes)
import GHC.Generics
import Unbound.Generics.LocallyNameless.Unify

data LambdaTerm = Var (Name LambdaTerm)
                | Meta (MetaVar LambdaTerm)
                | Lam (Bind (Name LambdaTerm) LambdaTerm)
                | App LambdaTerm LambdaTerm
                | Const String
                -- As of https://www.seas.upenn.edu/~sweirich/papers/icfp11.pdf (section 3.3)
                | Let (Bind (Rec [(Name LambdaTerm, Embed LambdaTerm)]) LambdaTerm)
                deriving (Show, Generic)

instance Alpha LambdaTerm
instance Subst LambdaTerm LambdaTerm where
  isvar (Var v) = Just $ SubstName v
  isvar _       = Nothing
instance MetaSubst LambdaTerm LambdaTerm where
  isMetaVar (Meta v) = Just $ MetaSubstName v
  isMetaVar _        = Nothing
instance NormalForm LambdaTerm where
  nf v@(Var   _) = return v
  nf v@(Meta  _) = return v
  nf v@(Const _) = return v
  nf (Lam b)     = do (x, e) <- unbind b
                      e' <- nf e
                      return $ Lam (bind x e')
  nf (Let b)     = do (unrec -> xs, e) <- unbind b
                      e' <- nf e
                      xs' <- forM xs $ \(x, unembed -> t) -> do
                               t' <- nf t
                               return (x, embed t')
                      return $ Let (bind (rec xs') e')
  nf (App f e)   = do f' <- nf f
                      e' <- nf e
                      case f' of
                        Lam b -> do (x, b') <- unbind b
                                    nf (subst x e' b')
                        Let b -> do (x, b') <- unbind b
                                    n' <- nf (App b' e')
                                    return $ Let (bind x n')
                        _     -> return $ App f' e'
instance Unif LambdaTerm where
  split (App f e) = (++ [e]) <$> split f
  split t         = (t, [])
  unsplit t []     = t
  unsplit t (x:xs) = unsplit (App t x) xs
  unsplitMeta v    = unsplit (Meta v)
  abstract = Lam
  unabstract (Lam b) = Just b
  unabstract _       = Nothing

{-
maxExpandedUsageOf :: Name LambdaTerm -> LambdaTerm -> Maybe Int
maxExpandedUsageOf _ (Meta _) = Nothing
maxExpandedUsageOf _ (Let  _) = Nothing
maxExpandedUsageOf v (Lam  b) = runFreshM $ do (_, e) <- unbind b
                                               return $ maxExpandedUsageOf v e
maxExpandedUsageOf v (split -> (h, args))
  | Var w <- h, v == w
  = Just (length args)
  | otherwise
  = let uses = map (maxExpandedUsageOf v) (h : args)
    in case catMaybes uses of
         []    -> Nothing
         uses' -> Just $ maximum uses'
-}

etaExpandUpTo :: Fresh m => Int -> LambdaTerm -> m LambdaTerm
etaExpandUpTo n t
  | n <= 0
  = return t
  | Lam b <- t
  = do (x, e) <- unbind b
       e' <- etaExpandUpTo (n-1) e
       return $ Lam (bind x e')
  | otherwise
  = do xs <- replicateM n (fresh (s2n "x"))
       t' <- nf (unsplit t (map Var xs))
       return $ abstractMany xs t'

-- Here we assume that the terms are eta-expanded enough
swapLambdas :: forall m a. (Fresh m, Eq a) => [a] -> [a] -> LambdaTerm -> m LambdaTerm
swapLambdas perm1 perm2 t
  | length perm1 /= length perm2
  = error $ "The permutation lists are not the same"
  | otherwise
  = do let n = length perm1  -- which is equal to that of perm2
       (xs, e) <- unabstractMany n t
       let xs' = swapThings perm1 xs
       return $ abstractMany xs' e
  where
    swapThings :: [a] -> [b] -> [b]
    swapThings []     _    = []     
    swapThings (i:is) elts = case elemIndex i perm2 of
                               Nothing -> error "The lists are not permutations of one another"
                               Just ix -> (elts !! ix) : swapThings is elts